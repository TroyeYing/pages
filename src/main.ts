import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import { createPinia } from 'pinia'
App.use(createPinia())
App.use(ElementPlus)
createApp(App).mount('#app')



